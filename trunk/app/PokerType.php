<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PokerType extends Model
{
    protected $table = "poker_type";
    protected $fillable = ['key', 'name', 'card_data'];

    public function distributedSessions()
    {
        return $this->hasMany('\App\DistributedSession');
    }
}
