<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public function __construct()
    {
			
    }

    public function index(Request $request)
    {
        $data = array();
        $data['modalType'] = "exampleModal";
				$page = $request->type;

				switch($page){
					case "fibonacci":
						$view = "fibonacci";
						break;
					case "custom":
						$view = "custompoker";
						break;
					case "tshirt":
						$view = "tshirt";
						break;
					default :
						$view = "realnumber";
						break;
				}

        return view($view, $data);
    }

    public function login()
    {
			$this->middleware('auth');
			return view('home');
    }
}
