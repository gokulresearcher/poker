<?php

namespace App\Http\Controllers;
use App\PokerType;
use App\DistributedSession;
use App\DistributedSessionUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DistributedPokerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboardu.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = array();
        $data['pokerType'] = PokerType::orderBy('name')->pluck('name', 'id')->prepend('Select', '');
        return view('poker.remotePoker', $data);
    }

    public function createSession(Request $request)
    {
        //TODO : Cannot create more than 5 sessions per day
        $distributedSession = new DistributedSession;
        $distributedSession->key = mt_rand(100000, 999999);
        $distributedSession->title = $request->title;
        $distributedSession->poker_type_id = $request->poker_type_id;
        $distributedSession->user_id = Auth::id();
        $distributedSession->start_time = date('Y-m-d H:i:s');
        $distributedSession->end_time =  date('Y-m-d H:i:s', strtotime('+24 hours'));
        $distributedSession->save();

        $data = array();
				$data['key'] = $distributedSession->key;
        return response()->json(array('error'=>false, 'data'=>$data, 'status_code'=>200));
    }

    public function joinSession(Request $request)
    {
				//TODO : Check number of connections : max allow 10 per session
        if (Auth::check()) {
            $distributedSession = DistributedSession::where('key', $request->key)->first();
            if ($distributedSession->id > 0) {
                $distributedSessionUser = array();
                $distributedSessionUser['user_id'] = Auth::id();
                $distributedSessionUser['distributed_session_id'] = $distributedSession->id;
                DistributedSessionUser::updateOrCreate($distributedSessionUser);
            }

            $data = array();
            $data['key'] = $request->key;
						switch($distributedSession->poker_type_id){
							case "2":
								$view = "fibonacci";
								break;
							case "4":
								$view = "custompoker";
								break;
							case "3":
								$view = "tshirt";
								break;
							default :
								$view = "realnumber";
								break;
            }

            $data['modalType'] = "exampleModal";
            if (Auth::check()) {
                $data['modalType'] = "toastModal";
            }

						if (Auth::id() == $distributedSession->user_id) {
                $data['isSessionOwner'] = true;
						}

            return view($view, $data);
        }
    }

    public function updateStoryPoint(Request $request)
    {
        //TODO : Check number of connections : max allow 10 per session
				$returnData = array();
				$returnData['status'] = false;
				$returnData['message'] = "Invalid User / Session is expired";
        if (Auth::check()) {
            $distributedSession = DistributedSession::where('key', $request->key)->select('id', 'publish')->first();
            if ($distributedSession->id > 0 && $distributedSession->publish==0) {
                $distributedSessionUser = array();
                $distributedSessionUser['user_id'] = Auth::id();
                $distributedSessionUser['distributed_session_id'] = $distributedSession->id;
                $data = array();
                $data['story_point'] = $request->story_point;
                DistributedSessionUser::updateOrCreate($distributedSessionUser, $data);

								$returnData['status'] = true;
								$returnData['message'] = "Submitted story points successfully...";
            } else {
								$returnData['status'] = false;
								$returnData['message'] = "Failed to update, session-admin has freezed the poker.";
						}
						$returnData['key'] = $request->key;
						$returnData['story_point'] = $request->story_point;

						return response()->json(array('error'=>false, 'data'=>$returnData, 'status_code'=>200));
        }
    }

    public function clearStoryPoints(Request $request)
    {
				$data = array();
				$data['status'] = false;
				$data['message'] = "Invalid User / Session is expired";
        if (Auth::check()) {
            $distributedSession = DistributedSession::where('key', $request->key)
                                  ->where('user_id', Auth::id())->select('id', 'user_id')->first();
            if ($distributedSession->id > 0 && $distributedSession->user_id==Auth::id()) {
							DistributedSession::where('id', $distributedSession->id)->update(['publish'=>0]);
							DistributedSessionUser::where('distributed_session_id', $distributedSession->id)->update(['story_point'=>'']);
							$data['status'] = true;
							$data['message'] = "Reseted successfully.";
            }
        }
				return response()->json(array('error'=>false, 'data'=>$data, 'status_code'=>200));
    }

    public function publishStoryPoints(Request $request)
    {
				$data = array();
				$data['status'] = false;
				$data['message'] = "Invalid User / Session is expired";
        if (Auth::check()) {
            $distributedSession = DistributedSession::where('key', $request->key)
                                  ->where('user_id', Auth::id())->select('id', 'user_id')->first();
            if ($distributedSession->id > 0 && $distributedSession->user_id==Auth::id()) {
							DistributedSession::where('id', $distributedSession->id)->update(['publish'=>1]);
							$data['status'] = true;
							$data['message'] = "Now team members can see the pokered story points.";
            }
        }
				return response()->json(array('error'=>false, 'data'=>$data, 'status_code'=>200));
    }

    public function showResult(Request $request)
    {
				$data = array();
				$data['status'] = false;
				$data['message'] = "Ask session owner to publish data.";
        $distributedSession = DistributedSession::where('key', '=', $request->key)->first();						
        if ($distributedSession->id > 0 && $distributedSession->publish==1) {
            $dsu = $distributedSession->distributedSessionUsers()->select('*')->where('distributed_session_id', '=', $distributedSession->id )->get();
            foreach($dsu as $r) {
								$data['userdata'][] = ['name' => $r->name, 'story_point' => $r->story_point];
            }
						$data['status'] = true;
        }

				return response()->json(array('error'=>false, 'data'=>$data, 'status_code'=>200));
    }
}
