<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PokerType;

class PokerTypeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function get(Request $request)
    {
        if ($request->id) {
            $data = PokerType::find($request->id);
        } else {
            $data = PokerType::where('key', 'PROW');
        }

        ob_start();
        var_dump($data);
        $data = ob_get_clean();
        $fp = fopen("/home/emacs/file.txt", "w");
        fwrite($fp, $data);
        fclose($fp);


        return view('poker.deck', ['cardDetails' => $data]);
    }
}
