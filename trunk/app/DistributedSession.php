<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DistributedSession extends Model
{
    protected $table = "distributed_session";
    protected $fillable = ['title', 'key', 'start_time', 'end_time', 'user_id', 'poker_type_id'];

    public function users()
    {
        return $this->belongsTo('\App\User', 'user_id', 'id');
    }

    public function pokerTypes()
    {
        return $this->belongsTo('\App\PokerType', 'poker_type_id');
    }

    public function distributedSessionUsers()
    {
        return $this->belongsToMany('\App\User', 'distributed_session_user', 'distributed_session_id', 'user_id') ;
    }

}
