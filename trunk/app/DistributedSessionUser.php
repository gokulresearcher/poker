<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DistributedSessionUser extends Model
{
    protected $table = "distributed_session_user";
    protected $fillable = ['distributed_session_id', 'user_id', 'story_point'];
/*
    public function distributedSessions()
    {
        return $this->belongsToMany('\App\User', 'distributed_session_user', 'user_id', 'distributed_session_id');
    }

    public function users()
    {
        return $this->belongsToMany('\App\DistributedSession', 'distributed_session_user', 'distributed_session_id', 'user_id') ;
    }
 */
}
