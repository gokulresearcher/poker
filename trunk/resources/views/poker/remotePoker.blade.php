@extends('layouts.app')

@section('content')
<div class="row" id="remote-poker">

	<div class="col-md-6">
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">Create Session</h3>
			</div>
			<form role="form" id="launchsession" name="launchsession" action="generatesession" method="post">
				<div class="box-body">
					<div class="form-group">
						{{Form::token()}}
						<label for="title">Title</label>
						{{Form::text('title', null, array('placeholder'=>'Sprint-58', 'class'=>'form-control'))}}
					</div>
					<div class="form-group">
						<label for="exampleInputPassword1">Poker Card Type</label>
						{{Form::select('poker_type_id', $pokerType, null, array('class'=>'form-control'))}}
					</div>
				</div>
				<div class="box-footer">
					<button type="submit" class="btn btn-primary">Create</button>
						<div class="help-block result-container"></div>
				</div>
			</form>
		</div>

		<div class="box box-success">
			<div class="box-header with-border">
				<h3 class="box-title">Join Session</h3>
			</div>
			<form role="form" id="joinsession" name="joinsession" action="joinsession" method="post">
				<div class="box-body">
					<div class="form-group">
						{{Form::token()}}
						<label for="key">Session Code</label>
						{{Form::text('key', null, array('placeholder'=>'jER17E3', 'class'=>'form-control'))}}
					</div>
				<div class="box-footer">
					<button type="submit" class="btn btn-success">Join</button>
				</div>
			</form>
		</div>
	</div>

</div>   
@endsection
