@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 col-sm-12 col-sm-offset-0 col-xs-12 col-xs-offset-0">
                <div class="panel panel-default panel-body">
                    <div>
                      <form name="sendstorypoint" action="sendstorypoint" method="post">
                        {{Form::token()}}
                        <div>
                        {{Form::label('Story Point')}}
                        {{Form::text('story_point')}}
                        {{Form::hidden('key', $key)}}
                        </div>

                        {{Form::submit('Go')}}
                      </form>

                </div>
       </div>
    </div>
@endsection
