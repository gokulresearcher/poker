
		<!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar Menu -->

      <ul class="sidebar-menu">
        <li class="header">Menu</li>
        <!-- Optionally, you can add icons to the links -->
        <li class="treeview">
          <a href="#"><i class="fa fa-hand-paper-o"></i> <span>Poker</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{route('pokertype', ['type' => 'realnumber'])}}"><i class="fa fa-sort-numeric-asc"></i> Real Number</a></li>
            <li><a href="{{route('pokertype', ['type' => 'fibonacci'])}}"><i class="fa fa-sort-numeric-asc"></i> Fibonacci</a></li>
            <li><a href="{{route('pokertype', ['type' => 'tshirt'])}}"><i class="fa fa-sort-alpha-asc"></i> T-Shirt</a></li>
            <li><a href="{{route('pokertype', ['type' => 'custom'])}}"><i class="fa fa-edit"></i>Custom</a></li>
          </ul>
        </li>
        <li><a href="{{URL::to('remotepoker')}}"><i class="fa fa-users"></i> <span>Remote Poker</span></a></li>
        <li><a href="#"><i class="fa fa-newspaper-o"></i> <span>Scrum Events</span></a></li>
        <li><a href="#"><i class="fa fa-building"></i> <span>About Me</span></a></li>
        <li><a href="#"><i class="fa fa-book"></i> <span>Help</span></a></li>
      </ul>

      <!-- /.sidebar-menu -->
    </section>
