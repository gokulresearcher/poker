<div class="row">
	<div class="col-md-3 col-sm-3 col-xs-3" id="reset-points">
		<a class="btn btn-app colorred">
			<span id="reset-correct-icon" class="badge bg-green" style="display:none">Done</span>
			<span id="reset-failed-icon" class="badge bg-red" style="display:none">Failed</span>
      <i class="fa fa-eraser"></i>Reset
    </a>	
	</div>
	<div class="col-md-3 col-sm-3 col-xs-3 col-xs-offset-1" id="publish-points">
		<a class="btn btn-app colorgreen">
			<span id="publish-correct-icon" class="badge bg-green" style="display:none">Done</span>
			<span id="publish-failed-icon" class="badge bg-red" style="display:none">Failed</span>
      <i class="fa fa-users"></i>Publish
    </a>
	</div>
	<div class="col-md-3 col-sm-3 col-xs-3 col-xs-offset-1">
		Key is : {{$key}}
	</div>
</div>

<style type="text/css">
	a.colorred {
		color:red !important;
	}
	a.colorgreen {
		color:green !important;
	}
</style>
