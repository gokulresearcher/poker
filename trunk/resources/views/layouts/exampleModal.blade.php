	<div id="users-device-size">
		<div id="xs" class="visible-xs"></div>
		<div id="sm" class="visible-sm"></div>
		<div id="md" class="visible-md"></div>
		<div id="lg" class="visible-lg"></div>
	</div>


	<div class="exampleModal">
    <div class="modal" id="exampleModal">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-body">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
						</button>
						<div class="pokeredvalue"></div>
						</div>
					</div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
  </div>
  <!-- /.example-modal -->

  <style type="text/css">
.example-modal .modal {
  position: relative;
  top: auto;
  bottom: auto;
  right: auto;
  left: auto;
  display: block;
  z-index: 1;
}

.example-modal .modal {
  background: transparent !important;
}

	.modal-content {
  background-color: #f4f4f4;
	-webkit-border-radius: 0px !important;
  -moz-border-radius: 0px !important;
  border-radius: 15px !important;
  border-color: black;
  border-width: 3px;
	border-style: solid;
}

.toast-modal .modal {
  position: relative;
  top: auto;
  bottom: auto;
  right: auto;
  left: auto;
  display: block;
  z-index: 1;
}

.modal {
	text-align: center;
}

@media screen and (min-width: 768px) {
	.modal:before {
		display: inline-block;
		vertical-align: middle;
		content: " ";
		height: 100%;
	}
}

.modal-dialog {
	display: inline-block;
	text-align: center;
	vertical-align: middle;
}
  </style>
