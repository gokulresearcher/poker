	<div id="users-device-size">
		<div id="xs" class="visible-xs"></div>
		<div id="sm" class="visible-sm"></div>
		<div id="md" class="visible-md"></div>
		<div id="lg" class="visible-lg"></div>
	</div>


	<div class="toastModal">
    <div class="modal fade" id="toastModal">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-body">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
						</button>

						@if (!empty($key))
							{{Form::token()}}
							<input type="hidden" name="key" value="{{$key}}" id="sessionkey"/>
						@endif

						<div class="pokeredvalue"></div>
						<hr>
						<div id="team-story-point"></div>
						</div>
		        <!-- /.modal-body -->

						@if(!empty($isSessionOwner) && $isSessionOwner)
						<div class="modal-footer">
							<button type="button" class="btn btn-primary pull-right" id="modal-publish-button">Publish</button>
						</div>
						@endif

		        <!-- /.modal-footer -->
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
  </div>
  <!-- /.toast-modal -->

  <style type="text/css">
.example-modal .modal {
  position: relative;
  top: auto;
  bottom: auto;
  right: auto;
  left: auto;
  display: block;
  z-index: 1;
}

.example-modal .modal {
  background: transparent !important;
}

	.modal-content {
  background-color: #000;
	-webkit-border-radius: 0px !important;
  -moz-border-radius: 0px !important;
  border-radius: 15px !important; 
  border-color: white;
  border-width: 3px;
	border-style: solid;
}

.toast-modal .modal {
  position: relative;
  top: auto;
  bottom: auto;
  right: auto;
  left: auto;
  display: block;
  z-index: 1;
}

.modal {
	text-align: center;
}

@media screen and (min-width: 768px) { 
	.modal:before {
		display: inline-block;
		vertical-align: middle;
		content: " ";
		height: 100%;
	}
}

.modal-dialog {
	display: inline-block;
	text-align: center;
	vertical-align: middle;
}

.modal.fade .modal-dialog {
    -webkit-transform: scale(0.1);
    -moz-transform: scale(0.1);
    -ms-transform: scale(0.1);
    transform: scale(0.1);
    top: 300px;
    opacity: 0;
    -webkit-transition: all 0.3s;
    -moz-transition: all 0.3s;
    transition: all 0.3s;
}

.modal.fade.in .modal-dialog {
    -webkit-transform: scale(1);
    -moz-transform: scale(1);
    -ms-transform: scale(1);
    transform: scale(1);
    -webkit-transform: translate3d(0, -300px, 0);
    transform: translate3d(0, -300px, 0);
    opacity: 1;
}
  </style>
