@extends('layouts.app')

@section('content')

			@if(!empty($isSessionOwner) && $isSessionOwner)
				@include("layouts.resetPublish")
			@endif

    <div class="row">
        <div class="col-md-3 col-sm-3 col-xs-3">
            <span class="info-box-icon bg-aqua" data-toggle="modal" data-target="#{{$modalType}}" data-pokervalue="0"><i>0</i></span>
          <!-- /.info-box -->
        </div>

        <div class="col-md-3 col-sm-3 col-xs-3 col-xs-offset-1">
						<span class="info-box-icon bg-aqua" data-toggle="modal" data-target="#{{$modalType}}" data-pokervalue="0.5"><i>0.5</i></span>
          <!-- /.info-box -->
        </div>

        <div class="col-md-3 col-sm-3 col-xs-3 col-xs-offset-1">
						<span class="info-box-icon bg-aqua" data-toggle="modal" data-target="#{{$modalType}}" data-pokervalue="1"><i>1</i></span>
          <!-- /.info-box -->
        </div>
      </div>
			<br>
			<div class="row">
        <div class="col-md-3 col-sm-3 col-xs-3">
            <span class="info-box-icon bg-aqua" data-toggle="modal" data-target="#{{$modalType}}" data-pokervalue="2"><i>2</i></span>
          <!-- /.info-box -->
        </div>

        <div class="col-md-3 col-sm-3 col-xs-3 col-xs-offset-1">
						<span class="info-box-icon bg-aqua" data-toggle="modal" data-target="#{{$modalType}}" data-pokervalue="3"><i>3</i></span>
          <!-- /.info-box -->
        </div>

        <div class="col-md-3 col-sm-3 col-xs-3 col-xs-offset-1">
						<span class="info-box-icon bg-aqua" data-toggle="modal" data-target="#{{$modalType}}" data-pokervalue="5"><i>5</i></span>
          <!-- /.info-box -->
        </div>
      </div>
			<br>
			<div class="row">
        <div class="col-md-3 col-sm-3 col-xs-3">
            <span class="info-box-icon bg-aqua" data-toggle="modal" data-target="#{{$modalType}}" data-pokervalue="8"><i>8</i></span>
          <!-- /.info-box -->
        </div>

        <div class="col-md-3 col-sm-3 col-xs-3 col-xs-offset-1">
						<span class="info-box-icon bg-aqua" data-toggle="modal" data-target="#{{$modalType}}" data-pokervalue="13"><i>13</i></span>
          <!-- /.info-box -->
        </div>

        <div class="col-md-3 col-sm-3 col-xs-3 col-xs-offset-1">
						<span class="info-box-icon bg-aqua" data-toggle="modal" data-target="#{{$modalType}}" data-pokervalue="18"><i>18</i></span>
          <!-- /.info-box -->
        </div>
      </div>
			<br>
			<div class="row">
        <div class="col-md-3 col-sm-3 col-xs-3">
            <span class="info-box-icon bg-aqua" data-toggle="modal" data-target="#{{$modalType}}" data-pokervalue="23"><i>23</i></span>
          <!-- /.info-box -->
        </div>

        <div class="col-md-3 col-sm-3 col-xs-3 col-xs-offset-1">
						<span class="info-box-icon bg-aqua" data-toggle="modal" data-target="#{{$modalType}}" data-pokervalue="40"><i>40</i></span>
          <!-- /.info-box -->
        </div>

        <div class="col-md-3 col-sm-3 col-xs-3 col-xs-offset-1">
						<span class="info-box-icon bg-aqua" data-toggle="modal" data-target="#{{$modalType}}" data-pokervalue="80"><i>80</i></span>
          <!-- /.info-box -->
        </div>
      </div>
			<br>
			<div class="row">
        <div class="col-md-3 col-sm-3 col-xs-3">
            <span class="info-box-icon bg-aqua" data-toggle="modal" data-target="#{{$modalType}}" data-pokervalue="?"><i>?</i></span>
          <!-- /.info-box -->
        </div>

        <div class="col-md-3 col-sm-3 col-xs-3 col-xs-offset-1">
						<span class="info-box-icon bg-aqua" data-toggle="modal" data-target="#{{$modalType}}" data-pokervalue="inf"><i>inf</i></span>
          <!-- /.info-box -->
        </div>

        <div class="col-md-3 col-sm-3 col-xs-3 col-xs-offset-1">
						<span class="info-box-icon bg-aqua" data-toggle="modal" data-target="#{{$modalType}}" data-pokervalue="tea"><i>tea</i></span>
          <!-- /.info-box -->
        </div>
      </div>

    </section>

    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

	@include("layouts.$modalType")
@endsection
