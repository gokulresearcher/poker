@extends('layouts.app')

@section('content')

			@if(!empty($isSessionOwner) && $isSessionOwner)
				@include("layouts.resetPublish")
			@endif

 			<div class="row">
        <div class="col-md-3 col-sm-3 col-xs-3">
            <span class="info-box-icon bg-aqua" data-toggle="modal" data-target="#{{$modalType}}" data-pokervalue="XS"><i>XS</i></span>
          <!-- /.info-box -->
        </div>

        <div class="col-md-3 col-sm-3 col-xs-3 col-xs-offset-1">
						<span class="info-box-icon bg-aqua" data-toggle="modal" data-target="#{{$modalType}}" data-pokervalue="S"><i>S</i></span>
          <!-- /.info-box -->
        </div>

        <div class="col-md-3 col-sm-3 col-xs-3 col-xs-offset-1">
						<span class="info-box-icon bg-aqua" data-toggle="modal" data-target="#{{$modalType}}" data-pokervalue="M"><i>M</i></span>
          <!-- /.info-box -->
        </div>
      </div>
			<br>
			<div class="row">
        <div class="col-md-3 col-sm-3 col-xs-3">
            <span class="info-box-icon bg-aqua" data-toggle="modal" data-target="#{{$modalType}}" data-pokervalue="L"><i>L</i></span>
          <!-- /.info-box -->
        </div>

        <div class="col-md-3 col-sm-3 col-xs-3 col-xs-offset-1">
						<span class="info-box-icon bg-aqua" data-toggle="modal" data-target="#{{$modalType}}" data-pokervalue="XL"><i>XL</i></span>
          <!-- /.info-box -->
        </div>

        <div class="col-md-3 col-sm-3 col-xs-3 col-xs-offset-1">
						<span class="info-box-icon bg-aqua" data-toggle="modal" data-target="#{{$modalType}}" data-pokervalue="XXL"><i>XXL</i></span>
          <!-- /.info-box -->
        </div>
      </div>
			<br>
			<div class="row">
        <div class="col-md-3 col-sm-3 col-xs-3">
            <span class="info-box-icon bg-aqua" data-toggle="modal" data-target="#{{$modalType}}" data-pokervalue="?"><i>?</i></span>
          <!-- /.info-box -->
        </div>

        <div class="col-md-3 col-sm-3 col-xs-3 col-xs-offset-1">
						<span class="info-box-icon bg-aqua" data-toggle="modal" data-target="#{{$modalType}}" data-pokervalue="inf"><i>inf</i></span>
          <!-- /.info-box -->
        </div>

        <div class="col-md-3 col-sm-3 col-xs-3 col-xs-offset-1">
						<span class="info-box-icon bg-aqua" data-toggle="modal" data-target="#{{$modalType}}" data-pokervalue="tea"><i>tea</i></span>
          <!-- /.info-box -->
        </div>
      </div>

    </section>

    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

	@include("layouts.$modalType")
@endsection
