<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PokerType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('poker_type', function (Blueprint $table) {
            $table->increments('id');
            $table->string('key', 10)->unique();
            $table->string('name', 50);
            $table->text('card_data');
            $table->tinyInteger('deleted_at')->default(0);
            $table->timestamps();
        });

        $number = array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, '', '?', '');
        $number = json_encode($number);
        DB::table('poker_type')->insert(array('key'=>'NATU', 'name'=>'Natural', 'card_data'=>$number));

        $number = array(0, 1, 2, 3, 5, 8, 13, 21, 34, 55, '', '?', '');
        $number = json_encode($number);
        DB::table('poker_type')->insert(array('key'=>'FIBO', 'name'=>'Fibonacci', 'card_data'=>$number));

        $number = array('XS', 'S', 'M', 'L', 'XL', 'XXL', '', '?', '');
        $number = json_encode($number);
        DB::table('poker_type')->insert(array('key'=>'TSHI', 'name'=>'T-Shirt', 'card_data'=>$number));

        $number = array(0, '1/2', 1, 2, 3, 5, 8, 13, 20, 40, 80, '', '?', '');
        $number = json_encode($number);
        DB::table('poker_type')->insert(array('key'=>'PROW', 'name'=>'Prowareness', 'card_data'=>$number));

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('poker_type');
    }
}
