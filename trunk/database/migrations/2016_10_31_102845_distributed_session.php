<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DistributedSession extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('distributed_session', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 30);
            $table->string('key', 10)->unique();
            $table->dateTime('start_time');
            $table->dateTime('end_time');

            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');

            $table->integer('poker_type_id')->unsigned();
            $table->foreign('poker_type_id')->references('id')->on('poker_type');

            $table->tinyInteger('deleted_at')->default(0);
            $table->tinyInteger('publish')->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('distributed_session');
    }
}
