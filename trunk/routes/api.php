<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');

Route::post('generatesession', 'App\Http\Controllers\DistributedPokerController@createSession');
Route::post('sendstorypoint', 'App\Http\Controllers\DistributedPokerController@updateStoryPoint');
Route::get('showresult', 'App\Http\Controllers\DistributedPokerController@showResult');
Route::post('clearpoints', 'DistributedPokerController@clearStoryPoints');
Route::post('publishpoints', 'DistributedPokerController@publishStoryPoints');
