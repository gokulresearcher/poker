<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function(){
	return View::make('realnumber', ['modalType' => "exampleModal"]);
});

Route::get('/logout', 'Auth\LoginController@logout');
Route::get('pokertype/{type}', ['uses' => 'HomeController@index'])->name('pokertype');

Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    $exitCode = Artisan::call('optimize');
    $exitCode = Artisan::call('route:cache');
    $exitCode = Artisan::call('view:clear');
    $exitCode = Artisan::call('config:cache');
    Cache::flush();
    return "<h1>Cleared cache</h1>";
});

Auth::routes();

Route::get('/home', 'HomeController@login');

Route::get('getDeck', 'PokerTypeController@get');
Route::get('remotepoker', 'DistributedPokerController@index');
Route::post('generatesession', 'DistributedPokerController@createSession');
Route::post('joinsession', 'DistributedPokerController@joinSession');
Route::post('sendstorypoint', 'DistributedPokerController@updateStoryPoint');
Route::get('showresult', 'DistributedPokerController@showResult');
Route::post('clearpoints', 'DistributedPokerController@clearStoryPoints');
Route::post('publishpoints', 'DistributedPokerController@publishStoryPoints');

