//-- to know screen size --
	function getBootstrapDeviceSize() {
		return $('#users-device-size').find('div:visible').first().attr('id');
	}
//-- to know screen size END--

//-- bsmodal --
	$('#exampleModal').on('show.bs.modal', function (event) {
		var w = "auto";
		var h = "auto";
    singleChar = "2000";
    doubleChar = "1800";
    trippleChar = "1550";

		if(getBootstrapDeviceSize() =='xs' || getBootstrapDeviceSize() == 'sm') {
			w = parseInt(($(window).width()*window.devicePixelRatio));
			h = parseInt(($(window).height()*window.devicePixelRatio));

			singleChar = "2000";
			doubleChar = "1800";
			trippleChar = "1550";

			if (w >= 320 && w < 720) {
					w = 320 * 0.85;
			}

			if (w >= 720) {
					w = 720 * 0.4;
			}

			if (h >= 560 && h < 1080) {
					h = 560 * 0.3;

					singleChar = "380";
					doubleChar = "275";
					trippleChar = "25";
			}

			if (h >= 1080) {
					h = 1080 * 0.4;
			}
			w = w+'px';	h = h+'px';
		}

		$('.modal-body',this).css({width: w,height: h, 'max-height':'100%'});
		var target = $(event.relatedTarget);
		var pokerValue = ""+target.data('pokervalue');
		var pokerValueStrLen = pokerValue.length;
		var recipient = "<h1 style='font-size:"+singleChar+"%; padding-top:35px'>"+pokerValue+"</h1>";
		if (pokerValueStrLen==2)
			recipient = "<h1 style='font-size:"+doubleChar+"%; padding-top:35px'>"+pokerValue+"</h1>";
		if (pokerValueStrLen >= 3)
			recipient = "<h1 style='font-size:"+trippleChar+"%; padding-top:35px'>"+pokerValue+"</h1>";

		var modal = $(this);
		modal.find('.modal-body .pokeredvalue').html(recipient);
	});
//-- bsmodal END--

//-- toastmodal --
	$('#toastModal').on('show.bs.modal', function (event) {
		/*
		var modalTimer = $(this).delay(3000).fadeOut("slow", function () {
        $(this).modal('hide');
    });
		*/
		var w = "auto";
		var h = "auto";
		if(getBootstrapDeviceSize() =='xs' || getBootstrapDeviceSize() == 'sm') {
			w = parseInt(($(window).width()*window.devicePixelRatio));
			h = parseInt(($(window).height()*window.devicePixelRatio));
			if (w >= 320 && w < 720) {
					w = 320 * 0.85;
			}

			if (w >= 720) {
					w = 720 * 0.35;
			}

			if (h >= 560 && h < 1080) {
					h = 560 * 0.3;
			}

			if (h >= 1080) {
					h = 1080 * 0.4;
			}

			w = w+'px';	h = h+'px';
		}

		$('.modal-body',this).css({width: w,height: h, 'max-height':'100%'});
		$("#modal-publish-button").removeClass("btn-success btn-danger").addClass("btn-primary");
		var target = $(event.relatedTarget);
		var pokerValue = target.data('pokervalue');
		var sessionKey = $("#sessionkey").val();
		var token = $("input[name='_token'").val();
		var dataToSend = {key: sessionKey, story_point: pokerValue, "_token":token};
		var modal = $(this);

		var pokerValueStr = "You have pokered : <b>"+pokerValue+"</b>";
		var recipient = "<div><span style='color:white; padding-top:35px; font-size:14px'>"+pokerValueStr+"</span><p style='color:red; font-size:10px'><i>Submitting please wait...</i></p></div>";
		modal.find('.modal-body .pokeredvalue').html(recipient);

		$.ajax({
			type: "POST",
			url: "/sendstorypoint",
			data: dataToSend,
			success: function(result){
				if (!result.error) {
					var pokerValueStr = "You have pokered : <b>"+pokerValue+"</b>";
					if (result.data.status)
						var recipient = "<div><span style='color:white; padding-top:35px; font-size:14px'>"+pokerValueStr+"</span><p style='color:green; font-size:10px'><i>"+result.data.message+"</i></p></div>";
					else
						var recipient = "<div><span style='color:white; padding-top:35px; font-size:14px'>"+pokerValueStr+"</span><p style='color:red; font-size:10px'><b>"+result.data.message+"</b></p></div>";

					modal.find('.modal-body .pokeredvalue').html(recipient);

					$("#team-story-point").empty();
					$("#team-story-point").html("<h1 class='box-title' id='get-team-story-points'>Tap to see team's story point</h1>");

					$("#team-story-point").append("<span style='color:white' id='note-message'>Note : Ask session admin to publish team points, then tap on screen to see results.</span>");
				}
			}
		});
	});


$(document).on("click", "#get-team-story-points", function(){

	var data = {key:$("#sessionkey").val()};
	$.getJSON("showresult", data, function(){


	}).done(function(result){
		if (result.data.status) {
		$("#team-story-point").empty();
		  var str = "<div style='color:white'>";
			$.each(result.data.userdata, function(index, value){
				str +="<div class='row'>";
				str += "<div class='col-sm-6 col-xs-6 text-left'>"+value.name.toUpperCase()+"</div>";
				if (value.story_point != ''){
					str += "<div class='col-sm-1 col-xs-1'>"+value.story_point+"</div>";
				}else{
					str += "<div class='col-sm-1 col-xs-1'> - </div>";
				}
				str += "</div>";
			});
			str += "</div>";
			$("#team-story-point").append(str);
		} else {
			$("#note-message").css({"color":"red"});
		}
	});
});

$(document).on("click", "#modal-publish-button", function(event){
	var sessionKey = $("#sessionkey").val();
	var token = $("input[name='_token'").val();
	var dataToSend = {key: sessionKey, "_token":token};
  var btn = $(this);
	$.ajax({
		type: "POST",
		url: "/publishpoints",
		data: dataToSend,
		success: function(result){

			if (!result.error) {
				if (result.data.status) {
					btn.removeClass("btn-primary btn-danger").addClass("btn-success");
				} else {
					btn.removeClass("btn-primary btn-success").addClass("btn-danger");				}
			}
		}
	});
});
//-- toastmodal END--

//-- remote poker --
 $("#remote-poker #launchsession button").on("click", function(event){
		event.preventDefault();
		var err = 0;
		if($("input[name='title']").val() == ''){
			$("input[name='title']").closest("div.form-group").addClass("has-error");
		}
		if($("select[name='poker_type_id']").val() == ''){
			$("select[name='poker_type_id']").closest("div.form-group").addClass("has-error");
		}

		if(err==1){
			return;
		}

		$.ajax({
			type: "POST",
			url: "/generatesession",
			data: $("#launchsession").serialize(),
			success: function(result){
					$("#launchsession div").removeClass("has-error");
					$("#launchsession").trigger('reset');
					$("#launchsession .result-container").empty();
					$("#launchsession .result-container").append("<div class='alert alert-success alert-dismissible'>Your new session key is : <b>"+result.data.key+"</b></div>");
					$("#joinsession input[name='key'").val(result.data.key);
			}
		});
});
//-- remote poker END--

//-- resetPublish --
$("#reset-points").on("click", function(event){
	var sessionKey = $("#sessionkey").val();
	var token = $("input[name='_token'").val();
	var dataToSend = {key: sessionKey, "_token":token};
	$.ajax({
		type: "POST",
		url: "/clearpoints",
		data: dataToSend,
		success: function(result){
			if (!result.error) {
				if (result.data.status) {
					$("#reset-correct-icon").show(1500, function(){
						$(this).hide(2000);
					});
				} else {
					$("#reset-failed-icon").show(1500, function(){
						$(this).hide(2000);
					});
				}
			}
		}
	});
});

$("#publish-points").on("click", function(event){
	var sessionKey = $("#sessionkey").val();
	var token = $("input[name='_token'").val();
	var dataToSend = {key: sessionKey, "_token":token};
	$.ajax({
		type: "POST",
		url: "/publishpoints",
		data: dataToSend,
		success: function(result){

			if (!result.error) {
				if (result.data.status) {
					$("#publish-correct-icon").show(1500, function(){
						$(this).hide(2000);
					});
				} else {
					$("#publish-failed-icon").show(1500, function(){
						$(this).hide(2000);
					});
				}
			}
		}
	});
});
//-- resetPublish END--
